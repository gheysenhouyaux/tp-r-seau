#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <unistd.h>
#include <netdb.h>
#define PORT 4242
#define INVALID_SOCKET -1
#define SOCKET_ERROR -1
#define closesocket(s) close(s)

typedef int SOCKET;
typedef struct sockaddr_in SOCKADDR_IN;
typedef struct sockaddr SOCKADDR;
typedef struct in_addr IN_ADDR;

int main()
{
	SOCKET sock = socket(AF_INET, SOCK_STREAM, 0);
	struct hostent *hostinfo = NULL;
	SOCKADDR_IN sin = { 0 };
	const char *hostname = "10.102.26.116";
	char buffer[1024];
	char buffer[1024];
	int nombre1, nombre2;
	char operateur;
	int n = 0;

	scanf("%d %d %c", &nombre1, &nombre2, &operateur);
	// On écrit l'entier sur 4 bytes
 	buffer[0]= nombre1 & 0xff; //8 bits de poids faibles 
 	buffer[1]= (nombre1 >> 8) & 0xff;
 	buffer[2]= (nombre1 >> 16) & 0xff;
 	buffer[3]= (nombre1 >> 24) & 0xff; //8 bits de poids fort
 	buffer[4]= nombre2 & 0xff;
 	buffer[5]= (nombre1 >> 8) & 0xff;	
 	buffer[6]= (nombre1 >> 16) & 0xff;
 	buffer[7]= (nombre1 >> 24) & 0xff;
	buffer[8]= operateur;

	if(sock == INVALID_SOCKET)
	{
		perror("socket()");
		exit(errno);
	}
	hostinfo = gethostbyname(hostname);
	if(hostinfo == NULL)
	{
		fprintf(stderr, "Unknown host %s. \n",hostname);
		exit(EXIT_FAILURE);
	}
	
	sin.sin_addr = *(IN_ADDR *) hostinfo->h_addr;
	sin.sin_port = htons(PORT);
	sin.sin_family = AF_INET;
	
	if(connect(sock, (SOCKADDR *) &sin, sizeof(SOCKADDR)) == SOCKET_ERROR)
	{
		perror("connect()");
		exit(errno);
	}
	


	if(send(sock, buffer, strlen(buffer),0) < 0)
	{
		perror("send()");
		exit(errno);
	}
	
	if((n = recv(sock, response, sizeof buffer - 1,0)) < 0)
	{
		int nombreA = buffer[0] | ( (int)buffer[1] << 8 ) | ( (int)buffer[2] << 16 ) | ( (int)buffer[3] << 24 );
		printf ("%d", nombreA);
		perror("recv()");
		exit(errno);
	}
	buffer[n] = '\0';
	
	closesocket(sock);
	return 0;
}
