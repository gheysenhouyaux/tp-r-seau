#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#define PORT 4242
#define INVALID_SOCKET -1
#define SOCKET_ERROR -1
#define closesocket(s) close(s)

typedef int SOCKET;
typedef struct sockaddr_in SOCKADDR_IN;
typedef struct sockaddr SOCKADDR;
typedef struct in_addr IN_ADDR;

int main()
{
	SOCKET mySocket = socket(AF_INET, SOCK_STREAM,0);
	SOCKADDR_IN sin = { 0 };
	SOCKADDR_IN connectSin = { 0 };
	SOCKET connectSock;
	SOCKET sinsize = sizeof connectSin;

	if(mySocket == INVALID_SOCKET)
	{
		perror("socket()");
		exit(errno);
	}
	sin.sin_addr.s_addr = htonl(INADDR_ANY);
	sin.sin_family = AF_INET;
	sin.sin_port = htons(PORT);

	if(bind(mySocket, (SOCKADDR *) &sin, sizeof sin) == SOCKET_ERROR)
	{
		perror("bind()");
		exit(errno);
	}

	if(listen(mySocket, 5) == SOCKET_ERROR)
	{	
		perror("accept()");
		exit(errno);
	}

	connectSock = accept(mySocket,(SOCKADDR *) &connectSin, &sinsize);
	
	if(connectSock == INVALID_SOCKET)
	{
		perror("accept()");
		exit(errno);
	}
	closesocket(mySocket);
	closesocket(connectSock);
	
	return 0;
}
